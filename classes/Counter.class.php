<?php 

/**
 * Contador de tempo de acesso baseado em cookies
 * 
 * @version 1.0
 * @author Isaac Bruno <contato@ibruno.dev>
 * @link ibruno.dev
 */

class Counter {

    /** 
     * @var array
     * @access private
     **/ 
    private $config = [];

    /**
     * @return void
     */
    public function __construct() 
    {
        $this->setup();
    }

    /**
     * Insere uma configuração no array {@link $config}
     * 
     * @return Counter
     * @access public
     */
    public function setConfig( $index, $value = null )
    {
        $this->config[ $index ] = $value;

        return $this;
    }

    /**
     * Retorna uma configuração específica do array {@link $config}
     * 
     * @return mixed
     * @access public
     */
    public function getConfig( $index )
    {
        return $this->config[ $index ] ?? null;
    }

    /**
     * Retorna um array com todas as configurações {@link $config}
     * 
     * @return array
     * @access public
     */
    public function getConfigs()
    {
        return $this->config;
    }

    /**
     * Inicializa o funcionamento da classe. Pode ser usado para "resetar" a classe caso haja alguma mudança de configuração
     * 
     * @return Counter
     * @access private
     */
    private function setup()
    {
    /**
     * Definindo configurações padrão para funcionamento da classe
     */

        // define o nome do cookie
        if(!$this->getConfig('cookie_name')) $this->setConfig('cookie_name', '_counter');

        // define o path do cookie
        if(!$this->getConfig('cookie_path')) $this->setConfig('cookie_path', '/');

        // define a validade do cookie.
        // tempo atual + ( 1 dia em segundos * 365 ), ou um ano adiante
        if(!$this->getConfig('cookie_expiration')) $this->setConfig('cookie_expiration', time() + (86400 * 365 ) );

        // define o formato da data que será armazenada
        if(!$this->getConfig('date_format')) $this->setConfig('date_format', 'Y-m-d H:i:s');

        // define o tempo de contagem máxima para o usuário (em segundos). Padrão: 2 dias
        if(!$this->getConfig('user_time_limit')) $this->setUserTimeLimit( 86400 * 2 );

        return $this;
    }

    /**
     * Define o tempo de contagem máxima para o usuário, em segundos
     * 
     * @return Counter
     * @access public
     */
    public function setUserTimeLimit( int $seconds )
    {
        $this->setConfig('user_time_limit', $seconds );

        return $this;
    }

    /**
     * Busca o cookie armazenado, caso exista
     * 
     * @return string
     * @access private
     */
    private function getCookie()
    {
        return $_COOKIE[ $this->getConfig('cookie_name') ] ?? null;
    }

    /**
     * Define o cookie
     * 
     * @return boolean
     * @access private
     */
    private function setCookie( $value = null, $expiration = null  )
    {
        return setcookie( 
            // nome do cookie
            $this->getConfig('cookie_name'),

            // valor do cookie, padrão caso não seja definido
            null == $value ? date( $this->getConfig('date_format') ) : $value ,

            // expiração do cookie
            $expiration ? $expiration : $this->getConfig('cookie_expiration'),

            // path do cookie
            $this->getConfig('cookie_path')
        );
    }

    /**
     * Verifica se a contagem já foi inicializada.
     * 
     * @return boolean
     * @access public
     */
    public function isCounterStarted()
    {
        return $this->getCookie() != null;
    }

    /**
     * Inicia o contador
     * 
     * @return Counter
     * @access public
     */
    public function startCounter()
    {
        $this->setCookie();

        return $this;
    }

    /**
     * Retorna o tempo restante em segundos até a expiração
     * 
     * @return int
     * @access public
     */
     public function getTimeLeft() 
     {
        $curdate = strtotime(date('Y-m-d H:i:s'));
        $start = strtotime($this->getCookie());

        $max = $start + $this->getConfig('user_time_limit');

        if(!$this->getCookie() ) return false;
        
        return $max - $curdate;
     }

    /**
     * Verifica se o contador expirou
     * 
     * @return boolean
     * @access public
     */
    public function isCounterExpired()
    {
        $time_left = $this->getTimeLeft();

        return $time_left <= 0 && $time_left !== false;
    }

    /**
     * Retorna a data e hora de expiração do contador
     * 
     * @access public
     * @return string
     */
     public function  getExpirationDatetime( $format = 'Y-m-d H:i:s' )
     {
        $start = strtotime($this->getCookie());
        $max = $start + $this->getConfig('user_time_limit');

        return date( $format, $max );
     }


    /**
     * Encerra o contador e limpa o cookie
     * 
     * @return Counter
     * @access public
     */
    public function clear()
    {
        $this->setCookie(null, time() - 1 );
        
        return $this;
    }
}