<?php 
require 'classes/Counter.class.php';

$counter = new Counter();

if( $_GET['clear']??null ) { $counter->clear(); header('location: http://localhost/contador'); die;    };

$counter->setUserTimeLimit(86400);

if( !$counter->isCounterStarted() ) $counter->startCounter();

else {
    echo '<h2> Tempo restante: ', $counter->getTimeLeft(), ' - data de expiração: ', $counter->getExpirationDatetime('d/m/Y H:i:s'),'</h2>';

    var_dump( $_COOKIE );

    if( $counter->isCounterExpired() ) echo '<h1 style="color:Red">Expirado</h1>';
}