# Contador de timeout baseado em cookie

O contador registrará a data de início da contagem e retornará o tempo restante e a expiração, baseando-se no tempo pré-determinado.

## Inicializando a classe

```
<?php 

require 'classes/Counter.class.php'; // importa o arquivo da classe

$counter = new Counter(); // instancia a classe
```

## Métodos

### setConfig( string $index, string $value )
Permite sobrescrever as seguintes configurações para a classe:
- **cookie_name**: identificador do cookie. Padrão: *'_counter'*
- **cookie_path**: path onde o cookie ficará disponível. É útil caso você deseje permitir o cookie apenas em um subdiretório específico. Padrão: *'/' (root)* 
- **cookie_expiration**: tempo em **segundos** para a expiração do cookie. Padrão: `time() + (86400 * 365 )`,  (365 dias a partir do momento de criação do cookie - 1 dia = 86400 segundos)
- **date_format**: formatação da data de início da contagem, que será armazenada no cookie. Padrão: *'Y-m-d H:i:s'*
- **user_time_limit**: tempo máximo em **segundos** que será feita a contagem. Padrão: *86400 * 2 (ou 48h)*.
    - Essa configuração pode ser definida usando o método setUserTimeLimit( int $seconds )

#### Exemplo de aplicação
```
$counter->setConfig('cookie_name', '_cookie_de_contagem')
    ->setConfig('cookie_path', '/site-novo');

$counter->setConfig('cookie_expiration', 86400 * 3 ); // cookie irá sumir após 72h
```

### getConfig( string $index )
Retorna o valor de uma das configurações listadas acima, passando o índice desejado

```
$cookie_name = $counter->getConfig('cookie_name'); // retorna '_counter'
```

### getConfigs()
Retorna um *array* com todas as configurações listadas acima

### setUserTimeLimit( int $seconds )
Configura o tempo máximo do contador, em segundos.

```
$counter->setUserTimeLimit( 86400 * 2 ); // contador definido para 48 horas
```

### isCounterStarted()

Verifica se a contagem de tempo foi iniciada

```
if( $counter->isCounterStarted() ) {
    echo 'Contagem iniciada';
} else {
    echo 'Contagem NÃO iniciada';
}
```

### startCounter()

O método define o cookie com a hora atual e inicia a contagem regressiva do tempo.

```
if( !$counter->isCounterStarted() ) {
    $counter->startCounter();
}
```

### getTimeLeft()

Retorna o tempo restante da contagem. 
> **Atenção**:
> O método pode retornar valores positivos para tempo restante, negativos para o tempo expirado, zero e `false` quando o contador não foi iniciado.
> Portanto, é recomendado verificar o tempo restante com ===, <== ou >== para evitar confusões entre 0 e false

### getExpirationDatetime( string $format )

Retorna a data e/ou hora de expiração do contador, baseada na hora inicial + tempo máximo do contador defiido em `setUserTimeLimit()`. Esse método recebe uma string para formatação da data de saída, baseado no padrão de formatação de `date()` do PHP. Padrão: 'Y-m-d H:i:s'

```
echo $counter->getExpirationDatetime( 'd/m/Y H:i');
```

### isCounterExpired()

Verifica se o contador expirou após *n* segundos.

```
if( $counter->isCounterExpired() ) echo 'O contador chegou ao fim';
```

### clear()

Zera totalmente o contador para o usuário

```
$counter->clear();
```

## Exemplo

```
require __DIR__ . '/classes/Counter.class.php'; // importa a classe

$counter = new Counter(); // instância do contador

if( $_GET['clear'] ?? null ) { // se parâmetro GET for enviado 
    $counter->clear(); // limpa o contador 
    header('location: http://localhost/contador'); // recarrega a tela redirecionando o usuário
    die;    // para a execução
};

$counter->setUserTimeLimit(20); // define o tempo de 20 segundos para o contador

if( !$counter->isCounterStarted() ) {
    $counter->startCounter(); 
    // se o contador não foi iniciado inicia a contagem. esse IF é obrigatório
    // caso não seja feito cada vez que a tela for recarregada o contador reiniciará
} else {
    echo '<h2> Tempo restante: ', $counter->getTimeLeft(), '</h2>'; // exibe o tempo restante do contador

    if( $counter->isCounterExpired() ) echo '<h1 style="color:Red">Expirado</h1>'; // exibe se o contador foi expirado
}
```